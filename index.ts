export namespace check {
	/**
	 * Check wheher the input is a void
	 * @param {any} input Input to test
	 * @returns {boolean} Input is a void
	 */
	export function isVoid(input: any): input is void {
		return typeof input === "undefined" || input === null;
	}

	/**
	 * Check wheher the input is not a void
	 * @param {any} input Input to test
	 * @returns {boolean} Input is not a void
	 */
	export function isDefined(input: any): boolean {
		return !isVoid(input);
	}

	// ***

	/**
	 * Check whether the object is of provided class
	 * @param {class} _class Class to compare against
	 * @param {any} input Input to test
	 */
	export function is<Class extends Function>(_class: Class, input: any): boolean {
		return input instanceof _class || "name" in (_class) && typeof input === `${ _class.name }`.toLowerCase();
	}

	/**
	 * Check whether the input is a number
	 * @param {any} input Input to test
	 * @returns {boolean} Input is a number
	 */
	export function isNumber(input: any): input is number {
		return is(Number, input);
	}

	/**
	 * Check whether the input is a string
	 * @param {any} input Input to test
	 * @returns {boolean} Input is a string
	 */
	export function isString(input: any): input is string {
		return is(String, input);
	}

	/**
	 * Check whether the input is a boolean
	 * @param {any} input Input to test
	 * @returns {boolean} Input is a boolean
	 */
	export function isBoolean(input: any): input is boolean {
		return is(Boolean, input);
	}

	/**
	 * Check whether the input is a primitive
	 * @param {any} input Input to test
	 * @returns {boolean} Input is a primitive
	 */
	export function isPrimitive(input: any): boolean {
		return isVoid(input) || !(input instanceof Object);
	}

	// ***

	/**
	 * Check whether the number is finite
	 * @param {number} input Number to test
	 * @returns {boolean} Input is a finite number
	 */
	export function isFinite(input: number): boolean {
		return Number.isFinite(input);
	}

	/**
	 * Check whether the number is an integer
	 * @param {number} input Number to test
	 * @returns {boolean} Input is an integer
	 */
	export function isInteger(input: number): boolean {
		return Number.isInteger(input);
	}

	/**
	 * Check whether the number is divisible by an operand
	 * @param {number} input Number to test
	 * @param {number} operand Denominator
	 * @returns {boolean} Input is divisible by an operand
	 */
	export function isDivisibleBy(input: number, operand: number): boolean {
		return !(input % operand);
	}

	/**
	 * Check whether the number is natural
	 * @param {number} input Number to test
	 * @param {boolean} zero (optional) Whether to consider zero as a natural number. Default is `true`
	 * @returns {boolean} Input is a natural number
	 */
	export function isNatural(input: number, zero: boolean = true): boolean {
		return isInteger(input) && input >= (+!zero);
	}

	type Range = [number, number];

	function toRange(numbers: number[]): Range {
		return <Range> [ Math.min(...numbers), Math.max(...numbers) ];
	}

	/**
	 * Check whether the number is in provided range
	 * @param {number[]} range The range
	 * @param {number} input Number to test
	 * @param {boolean} inclusively (optional) Whether to count boundary match. Default is `true`
	 * @returns {boolean} Input is in provided range
	 */
	export function isInRange(range: number[], input: number, inclusively: boolean = true): boolean {
		const [ min, max ] = toRange(range);

		return inclusively? (input >= min && input <= max) : (input > min && input < max);
	}

	// ***

	/**
	 * Check whether the string does start with the specified substring
	 * @param {string} input String to test
	 * @param {string} substring Substring to compare against
	 * @param {number} position (optional) Starting position
	 * @returns {boolean} Input starts with the specified substring
	 */
	export function startsWith(input: string, substring: string, position?: number): boolean {
		return input.startsWith(substring, position);
	}

	/**
	 * Check whether the string does end with the specified substring
	 * @param {string} input String to test
	 * @param {string} substring Substring to compare against
	 * @param {number} endPosition (optional) Ending position
	 * @returns {boolean} Input ends with the specified substring
	 */
	export function endsWith(input: string, substring: string, endPosition?: number): boolean {
		return input.endsWith(substring, endPosition);
	}

	/**
	 * Check whether the string does include the specified substring
	 * @param {string} input String to test
	 * @param {string} substring Substring to compare against
	 * @param {number} position (optional) Starting position
	 * @returns {boolean} Input includes the specified substring
	 */
	export function includes(input: string, substring: string, position?: number): boolean {
		return input.includes(substring, position);
	}

	/**
	 * Check whether the string matches against the specified pattern
	 * @param {string} input String to test
	 * @param {RegExp} pattern Pattern to match against
	 * @returns {boolean} Input matches the specified pattern
	 */
	export function matches(input: string, pattern: RegExp): boolean {
		return pattern.test(input);
	}
}
